import models.Rectangle;
import models.Triangle;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle readable1 = new Rectangle("red", 1, 2);
        Rectangle readable2 = new Rectangle("blue", 2, 3);
        System.out.println(readable1);
        System.out.println(readable2);
        System.out.println(readable1.getArea());
        System.out.println(readable2.getArea());

         Triangle triangle1 = new Triangle("green", 6, 1);
        Triangle triangle2 = new Triangle("yellow", 7, 1);
        System.out.println(triangle1);
        System.out.println(triangle2);
        System.out.println(triangle1.getArea());
        System.out.println(triangle2.getArea());

    }
}
